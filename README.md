# Deceptive Mazes Testbed for Divergent Search

This testbed contains 60 procedurally generated mazes.

Mazes are generated via a recursive division algorithm, which subdivides the maze (by adding walls at the border) recursively until no more walls can be added. 
The algorithm stops after a specific number of subdivisions,
or when adding a new wall would make the path non-traversable. 
The start position is always set on the bottom-left corner and the goal position on the top-right corner.
Width of gaps and the minimum width of corridors is defined in a way that allows the robot controller to comfortably pass through.
All mazes tested have between 2 and 6 subdivisions (chosen randomly).

# Visualization
	
The repository contains a parser (ShowMaze.py) to save the mazes as .png file.

### Python Requirements 

* Python 3.6
* Matplotlib >= 2.02

# Additional Notes

The selected maze descriptor is based on the maze encoding used in the original novelty search source code provided by Joel Lehman and Kenneth O. Stanley [1].

# References

[1] Lehman, Joel, and Kenneth O. Stanley. "Abandoning objectives: Evolution through the search for novelty alone." Evolutionary computation 19.2 (2011): 189-223.